(* This file is generated by Why3's Coq driver *)
(* Beware! Only edit allowed sections below    *)
Require Import BuiltIn.
Require BuiltIn.
Require int.Int.

(* Why3 assumption *)
Inductive taskview :=
  | SigWait : Z -> Z -> taskview
  | SigOnly : Z -> taskview
  | WaitOnly : Z -> taskview.
Axiom taskview_WhyType : WhyType taskview.
Existing Instance taskview_WhyType.

(* Why3 assumption *)
Inductive signal_phase : taskview -> Z -> Prop :=
  | signal_phase_sw : forall (s:Z) (w:Z), (signal_phase (SigWait s w) s)
  | signal_phase_so : forall (s:Z), (signal_phase (SigOnly s) s).

Axiom signal_phase_deterministic : forall (tv:taskview) (i1:Z) (i2:Z),
  (signal_phase tv i1) -> ((signal_phase tv i2) -> (i1 = i2)).

(* Why3 assumption *)
Inductive wait_phase : taskview -> Z -> Prop :=
  | wait_phase_sw : forall (s:Z) (w:Z), (wait_phase (SigWait s w) w)
  | wait_phase_wo : forall (w:Z), (wait_phase (WaitOnly w) w).

Axiom wait_phase_deterministic : forall (tv:taskview) (i1:Z) (i2:Z),
  (wait_phase tv i1) -> ((wait_phase tv i2) -> (i1 = i2)).

(* Why3 assumption *)
Inductive facilitates : taskview -> taskview -> Prop :=
  | faciliates_wo : forall (i:Z) (tv:taskview), (facilitates (WaitOnly i) tv)
  | facilitates_so : forall (tv:taskview) (i:Z), (facilitates tv (SigOnly i))
  | facilitates_other : forall (tv1:taskview) (tv2:taskview) (i1:Z) (i2:Z),
      (signal_phase tv1 i1) -> ((wait_phase tv2 i2) -> ((i2 <= i1)%Z ->
      (facilitates tv1 tv2))).

(* Why3 assumption *)
Inductive impedes : taskview -> taskview -> Prop :=
  | impedes_def : forall (i1:Z) (i2:Z) (t1:taskview) (t2:taskview),
      (signal_phase t1 i1) -> ((wait_phase t2 i2) -> ((i1 < i2)%Z -> (impedes
      t1 t2))).

Axiom facilitates_impl_not_impedes : forall (tv1:taskview) (tv2:taskview),
  (facilitates tv1 tv2) -> ~ (impedes tv1 tv2).

Axiom impedes_impl_not_facilitates : forall (tv1:taskview) (tv2:taskview),
  (impedes tv1 tv2) -> ~ (facilitates tv1 tv2).

Axiom l1 : forall (j:Z) (i:Z), ~ (signal_phase (WaitOnly j) i).

Axiom l2 : forall (i:Z), exists j:Z, (signal_phase (SigOnly i) j).

Axiom l3 : forall (i:Z) (j:Z), exists k:Z, (signal_phase (SigWait i j) k).

(* Why3 goal *)
Theorem l4 : forall (tv:taskview) (i:Z), (~ (signal_phase tv i)) ->
  exists j:Z, (tv = (WaitOnly j)).
(* Why3 intros tv i h1. *)
intros tv i.

Qed.

