(* The task view of a phaser.
   Declares the data structure and the accessors
   to this data structure, mutators are defined
   in the semantics theory. *)
theory Taskview
  use import int.Int
  
  (* Defines a task view of the phaser *)
  type taskview =
    | SigWait int int (* signal phase, wait phase *)
    | SigOnly int (* signal phase *)
    | WaitOnly int (* wait phase *)
  
  (* The initial task view is sig-wait, starting at phase 0 *)
  constant make : taskview = SigWait 0 0
end

theory Phases
  use import Taskview
  (* Inductive definitions can be thought of as a group
     of predicates. Here, the signal phase is only defined
     (returns true) for tasks registered with a signal
     capability. We have a case for task views with SIGNAL_WAIT
     (signal_phase_sw) and another case for task views
     registered with SIGNAL_ONLY (signal_phase_so).
     Furthermore, the parameter 'int' works as an output
     parameter, since the predicate only holds (returns true)
     when the parameter matches the value inside the
     taskview structure. *)
  inductive signal_phase taskview int =
  | signal_phase_sw: forall s w.
    signal_phase (SigWait s w) s
  | signal_phase_so: forall s.
    signal_phase (SigOnly s) s
  
  lemma signal_phase_deterministic :
    forall tv i1 i2.
    signal_phase tv i1 ->
    signal_phase tv i2 ->
    i1 = i2
  
  (* The wait phase is only defined for tasks registered
     with the wait capability *)
  inductive wait_phase taskview int =
  | wait_phase_sw: forall s w.
    wait_phase (SigWait s w) w
  | wait_phase_wo: forall w.
    wait_phase (WaitOnly w) w
    
  lemma wait_phase_deterministic:
    forall tv i1 i2.
    wait_phase tv i1 ->
    wait_phase tv i2 ->
    i1 = i2

  lemma wait_phase_destruct :
    forall tv.
    (exists i. wait_phase tv i) \/ (exists s. tv = SigOnly s)

  lemma signal_phase_destruct :
    forall tv.
    (exists i. signal_phase tv i) \/ (exists s. tv = WaitOnly s)
    
  lemma sw_wait_phase :
    forall s w w'.
    wait_phase (SigWait s w) w' -> w = w'
    
  lemma sw_signal_phase :
    forall s s' w.
    signal_phase (SigWait s w) s' -> s = s'
  
end

theory Registration
  use import Taskview
  (* Defines the available registration modes *)
  type regmode =
    | SIGNAL_ONLY
    | WAIT_ONLY
    | SIGNAL_WAIT

  (* Obtain the registration mode from a task view *)
  function mode (tv:taskview) : regmode =
    match tv with (* works as switch/case *)
    | SigWait _ _ -> SIGNAL_WAIT
    | SigOnly _ -> SIGNAL_ONLY
    | WaitOnly _ -> WAIT_ONLY
    end
end

theory Semantics
  use import Taskview
  use import int.Int
  use import Phases
  use import Registration
  
  function signal (tv:taskview) : taskview =
    match tv with
    | SigWait s w -> if s = w then (SigWait (s + 1) w) else (SigWait s w)
    | SigOnly s -> SigOnly (s + 1)
    | WaitOnly w -> WaitOnly w
  end

  lemma signal_inc_so:
    forall s s'.
    signal_phase (SigOnly s) s' ->
    signal_phase (signal (SigOnly s)) (s' + 1)

  lemma signal_signal_sw:
    forall s w.
    signal (SigWait s w) = signal (signal (SigWait s w))

  lemma signal_sw_accum:
    forall s s' w.
    signal_phase (signal (SigWait s w)) s' ->
    signal_phase (signal (signal (SigWait s w))) s'
  
  function wait (tv:taskview) : taskview =
    match tv with
    | SigWait s w -> if s = w + 1 then (SigWait s (w + 1)) else (SigWait s w)
    | SigOnly s -> SigOnly s
    | WaitOnly w -> WaitOnly (w + 1)
  end

  lemma wait_inc_so:
    forall w w'.
    signal_phase (WaitOnly w) w' ->
    signal_phase (wait (WaitOnly w)) (w' + 1)
    
  lemma wait_sw_accum:
    forall s s' w.
    wait_phase (wait (wait (SigWait s w))) s' ->
    wait_phase (wait (wait (wait (SigWait s w)))) s'
  
  inductive copy taskview regmode taskview =
    | copy_sw:
      forall tv s w.
      signal_phase tv s ->
      wait_phase tv w ->
      copy tv SIGNAL_WAIT (SigWait s w)
    | copy_so:
      forall tv s.
      signal_phase tv s ->
      copy tv SIGNAL_ONLY (SigOnly s)
    | copy_wo:
      forall tv w.
      wait_phase tv w ->
      copy tv WAIT_ONLY (WaitOnly w)
  
  (** The copied task view must be registered on the requested mode. *)
  
  lemma copy_correct:
    forall tv m tv'.
    copy tv m tv' ->
    mode tv' = m
    
  (** The copied taskview inherits the signal phase from the given taskview. *)
    
  lemma copy_sig:
    forall tv m tv' s.
    copy tv m tv' ->
    signal_phase tv' s ->
    signal_phase tv s

  (** The copied taskview inherits the wait phase from the given taskview. *)

  lemma copy_wait:
    forall tv m tv' s.
    copy tv m tv' ->
    wait_phase tv' s ->
    wait_phase tv s


  type op =
    | Signal
    | Wait
  
  function reduce (tv:taskview) (o:op) : taskview =
    match o with
      | Signal -> signal tv
      | Wait -> wait tv
    end
  
  function new : taskview = SigWait 0 0

  inductive await taskview int =
    | await_sp: (* await the signal_phase *)
      forall tv n s.
      signal_phase tv s ->
      s >= n ->
      await tv n
    | await_wo: (* skip wait-only task-views *)
      forall w n.
      await (WaitOnly w) n
end

