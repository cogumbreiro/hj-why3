#!/bin/sh
[ -z "$DOCKER" ] && DOCKER=docker
[ -z "$IMG" ] && IMG=cogumbreiro/hj-why3
[ -z "$HTTP_PORT" ] && HTTP_PORT=8080
[ -z "$USE_DOCKER" ] && USE_DOCKER=yes

set -x

VM="$(which docker-machine || which boot2docker)"

use_docker() {
    test "$USE_DOCKER" = yes
}

image_absent() {
    local result=$($DOCKER images --no-trunc=true | awk "{if (\$1==\"$1\") {print \$1}}")
    test -z "$result"
}

run_docker() {
    if image_absent $IMG; then
        $DOCKER pull $IMG
    fi
    $DOCKER run -it --rm -v $(pwd):/hj-why3 -e USE_DOCKER=no "$@"
}

run_docker_http() {
    if test -f "$VM" ; then
        # Open the port
        "$VM" ssh $("$VM" active) -- -vnNTL 8080:localhost:$HTTP_PORT &
        port_forward=$!
    fi
    run_docker -p 8080:$HTTP_PORT $IMG check "$@"
    if test -f "$VM" ; then
        kill $port_forward
    fi
}

run_docker_x11() {
    local HOST=localhost
    xhost +local:$HOST
    run_docker -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -h $HOST $IMG ./$(basename $0) prove "$@"
    RET=$?
    xhost -local:$HOST
    exit $?
}

run_why3ide() {
    why3 ide -L . "$@"
}


run_check_all() {
    if use_docker; then
        run_docker $IMG ./scripts/regtests.sh
    else
        exec ./scripts/regtests.sh
    fi
}

run_prove() {
    if use_docker; then
        run_docker_http "$@"
    else
        run_why3ide "$@"
    fi
}

run_prompt() {
    run_docker $IMG
}

main() {
    case "$1" in
      prove) shift; run_prove "$@" ;;
      check) run_check_all ;;
      prompt) run_prompt ;;
      *)
        echo $"Usage: $0 {prove|check|prompt}"
        exit 1
    esac
}

main "$@"
