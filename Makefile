WHY3 = why3
DOCKER = docker
DOCKER_IMAGE = hj-why3
RUNFLAGS = -p 8080:8080 -v $(PWD):/hj-why3 -e DISPLAY=$(DISPLAY) -v /tmp/.X11-unix:/tmp/.X11-unix -h localhost
RUNARGS =

all: check

check:
	./scripts/regtests.sh

docker:
	$(DOCKER) build -t $(DOCKER_IMAGE) .

rundocker: docker
	$(DOCKER) run -it --rm $(RUNFLAGS) $(DOCKER_IMAGE) $(RUNARGS)

.PHONY: all replay docker rundocker
