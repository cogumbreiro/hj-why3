#!/bin/bash
if [ "$#" -ge 1 ]; then
	startx ~/.xinitrc "$@" -- /usr/bin/Xvfb $DISPLAY -screen 0 1280x780x24
else
	./scripts/regtests.sh
fi
